import os
import shutil

from fastapi import FastAPI
from fastapi.responses import FileResponse
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import Select

from selenium.webdriver.common.keys import Keys

app = FastAPI()


@app.get("/")
async def root():
    return {"message": "HOLI"}


#HACER QUERY POR RUT PARA SABER AREA.
#VALIDAR FECHA DE FINIQUITO.
# VIGENCIA DE VINCULACIÓN (fecha_fin >= today() )  ALERTA RETURN. EXCEPTION.
#ESTADO VINCULADO O DESVICULACION


@app.get("/liquidacion/{mm}/{aaaa}/{rut}")
async def make_url(mm, aaaa, rut):

    username = 'ssaavedra'
    password = '176168390'
    url = 'https://redm.cmrenca.cl'



    download_dir = aaaa+"/"+mm
    chromeOptions = Options()
    profile = {"plugins.plugins_list": [{"enabled": False, "name": "Chrome PDF Viewer"}],  # Disable Chrome's PDF Viewer
               #"download.extensions_to_open": "applications/pdf",
               "download.default_directory": download_dir,
               #"excludeSwitches": "enable-logging",
               "plugins.always_open_pdf_externally": True}
    chromeOptions.add_experimental_option("prefs", profile)

    browser = webdriver.Chrome("C://Users/Sebastian/Desktop/chromedriver_win32/chromedriver.exe", chrome_options=chromeOptions)




    browser.get(url)
    browser.find_element_by_name("user-id").send_keys(username)
    browser.find_element_by_name("user-pw").send_keys(password)
    browser.find_element_by_name("enviar-login").click()

    browser.get(url + "/sistemasEduc/sis_remuCod/index_main.php?areasistema=M&idsistema=921")
    browser.get(url + "/sistemasEduc/sis_remuCod/informes/openHistorico.php?mod=921102")
    browser.find_element_by_name("botonRet").click()

    browser.get(url + "/sistemasEduc/sis_remuCod/informes/openHistorico.php?mod=921102")


    browser.find_element_by_name("aaaa").click()
    select = Select(browser.find_element_by_name('aaaa'))
    # select by value
    select.select_by_value(aaaa)




    browser.find_element_by_name("mm").click()
    select2 = Select(browser.find_element_by_name('mm'))
    # select by value
    select2.select_by_value(mm)


    #browser.find_element_by_xpath("/html/body/div[2]/div[2]/form/div[1]/div[1]/label/select").send_keys(mm)




    browser.find_element_by_name("botonHis").click()

    browser.get(url + "/sistemasEduc/sis_remuCod/informes/inf_liquidacion.php?mod=921403")
    browser.find_element_by_name("campo").send_keys('rut')
    browser.find_element_by_name("buscar").send_keys(rut)
    browser.find_element_by_xpath("/html/body/div[2]/div[2]/div[2]/div[8]/input").click()

    browser.find_element_by_class_name("enviarpeq").click()


    #SI EXISTE EL ARCHIVO DOC.PDF EN DOWNLOAD?
    if os.path.isfile('C://Users/Sebastian/Downloads/doc.pdf') is True:

        #MUEVE EL ARCHIVO DOC.PDF A PATH DEL PROYECTO
        shutil.move("C://Users/Sebastian/Downloads/doc.pdf", "C://Users/Sebastian/PycharmProjects/api_lme/"+download_dir)

        #RENOMBRA EL ARCHIVO COMO CORRESPONDE
        os.rename("C://Users/Sebastian/PycharmProjects/api_lme/" + download_dir + "/doc.pdf",
                  "C://Users/Sebastian/PycharmProjects/api_lme/" + download_dir + "/" + rut + ".pdf")



    else:
        print("NO EXISTE ARCHIVO")
    #browser.get(url + "/sistemasEduc/sis_remuCod/informes/inf_liquidacionPrint.php?IdRegistro="+rut)



    #browser.find_element_by_xpath("/html/body/pdf-viewer//viewer-toolbar//div/div[3]/viewer-download-controls").click()

    #return FileResponse(media_type='application/octet-stream', filename=download)

    #return FileResponse(pdf)
    #return {"MES": mm, "AÑO": aaaa, "RUT": rut}
    file_name = rut + "_" + mm + "_" + aaaa +".pdf"
    # DEPENDS ON WHERE YOUR FILE LOCATES
    file_path = "C://Users/Sebastian/PycharmProjects/api_lme/" + download_dir + "/" + rut + ".pdf"
    return FileResponse(path=file_path, media_type='application/octet-stream', filename=file_name)








@app.get("/hola")
async def main():
    file_name = "ejemplo_1.pdf"
    # DEPENDS ON WHERE YOUR FILE LOCATES
    file_path = os.getcwd() + "/" + file_name
    return FileResponse(path=file_path, media_type='application/octet-stream', filename=file_name)
